//bài 1
/* Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần
    input: tạo 3 input để user nhập value 
    progress: lấy value gán cho 3 biến. điều kiện > 0
              Nếu không k bằng dùng if else để lấy được từng vị trị trí lớn nhất hoặc nhỏ nhất.  
              So sánh 2 vị trí còn lại nếu bằng nhau thì xuất sau số bé nhất còn khác nhau thì xuất theo thứ tự từ bé đến lớn
    ouput: dùng innerHTML xuất ra ketQua nhận được.    
*/

document.getElementById('sapxep').onclick = function () {
    var x = document.getElementById("txt_so_nguyen1").value * 1;
    var y = document.getElementById("txt_so_nguyen2").value * 1;
    var z = document.getElementById("txt_so_nguyen3").value * 1;
    if (x > 0 && y > 0 && z > 0) {
        if (x > y && x > z) {
            if (y > z) {
                document.getElementById("result").innerText = (z + ", " + y + ", " + x);
            }
            else {
                document.getElementById("result").innerText = (y + ", " + z + ", " + x);
            }
        }
        else if (y > x && y > z) {
            if (x > z) {
                document.getElementById("result").innerText = (z + ", " + x + ", " + y);
            }
            else {
                document.getElementById("result").innerText = (x + ", " + z + ", " + y);
            }
        }
        else if (z > x && z > y) {
            if (x > y) {
                document.getElementById("result").innerText = (y + ", " + x + ", " + z);
            }
            else {
                document.getElementById("result").innerText = (x + ", " + y + ", " + z);
            }
        } else if (x === y && y === z) {
            document.getElementById("result").innerText = (x + ", " + y + ", " + z);
        }
    } else {
        alert("Xin mời bài nhập vào số nguyên lớn hơn 0");
    }
}
/**
 * Bài 2: Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ
input: user nhập vào input
progress: lấy value user nhập so sánh với điều kiện 
output: xuất ra câu chào tương ứng với điều kiện
 */

document.getElementById('aidangsudungmaytinh').onclick = function () {
    var tv = document.getElementById('tv').value;
    if (tv === 'b' || tv === 'B' || tv === 'bố' || tv === 'Bố') {
        document.getElementById('chao').innerHTML = `Chào buối sáng bố`
    } else if (tv === 'm' || tv === 'M' || tv === 'mẹ' || tv === 'Mẹ') {
        document.getElementById('chao').innerHTML = `Chào buối sáng mẹ`

    } else if (tv === 'a' || tv === 'A' || tv === 'anh' || tv === 'Anh') {
        document.getElementById('chao').innerHTML = `Chào buối sáng anh trai`

    } else if (tv === 'e' || tv === 'E' || tv === 'em' || tv === 'Em') {
        document.getElementById('chao').innerHTML = `Chào buối sáng em gái`
    }
}








//bài3
/* Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.
input: láy giá trị user nhập vào từ input
*/
document.getElementById('chanle').onclick = function () {
    var s1 = document.getElementById("so1").value * 1;
    var s2 = document.getElementById("so2").value * 1;
    var s3 = document.getElementById("so3").value * 1;
    if (s1 > 0 && s2 > 0 && s3 > 0) {
        if (s1 % 2 != 0 && s2 % 2 != 0 && s3 % 2 != 0) {
            document.getElementById('dem').innerHTML = `Có 3 số lẻ, 0 số chẵn`
        } else if ((s1 % 2 != 0 && s2 % 2 != 0) || (s2 % 2 != 0 && s3 % 2 != 0) || (s1 % 2 != 0 && s3 % 2 != 0)) {
            document.getElementById('dem').innerHTML = `Có 2 số lẻ, 1 số chẵn`
        } else if (s1 % 2 == 0 && s2 % 2 == 0 && s3 % 2 == 0) {
            document.getElementById('dem').innerHTML = `Có 0 số lẻ,3 số chẵn`
        } else if ((s1 % 2 == 0 && s2 % 2 == 0) || (s2 % 2 == 0 && s3 % 2 == 0) || (s1 % 2 == 0 && s3 % 2 == 0)) {
            document.getElementById('dem').innerHTML = `Có 1 số lẻ, 2 số chẵn`;
        }
    } else {
        document.getElementById('dem').innerHTML = `nhập số nguyên > 0`
    }

}



/**
 * Bài 4: Viết chương trình nhập vào 3 cánh của tam giác và cho biết đó là tam giác gì.
 * input: lấy value của user nhập vào.
 * progress: dùng if else xử lý
 *  + value > 0.
 *  + 3 cạnh bằng nhau => tam giác đều
 *  + 2 cạnh bằng nhau => tam giác cân
 *  + bình phương 1 cạnh bằng tổng bình phương 2 cạnh còn lại => tan giác vuông
 *  + 2 cạnh bằng nhau && tổng bình phương của 2 cạnh == bình phương 1 cạnh => tam giác vuông cân
 *  + Tam giác thường 
 * output: Xuất ra loại tam giác tương ứng với value mà user nhập vào
 */

document.getElementById('bai4-btn').onclick = function () {
    var canh1 = document.getElementById('canh1').value * 1;
    var canh2 = document.getElementById('canh2').value * 1;
    var canh3 = document.getElementById('canh3').value * 1;
    ketqua = '';
    if (canh1 + canh2 > canh3 && canh2 + canh3 > canh1 && canh3 + canh1 > canh2) {
        if (canh1 === canh2 && canh2 === canh3) {
            ketqua = 'tam giác đều';
        }
        else if (canh1 === canh2 || canh1 === canh3 || canh2 === canh3) {
            ketqua = 'tam giác cân';
        }
        else if (canh1 * canh3 === canh2 * canh2 + canh3 * canh3 || canh2 * canh2 === canh1 * canh1 + canh3 * canh3 || canh3 * canh3 === canh2 * canh2 + canh1 * canh1) {
            ketqua = 'tam giác vuông';
        } else {
            ketqua = 'tam giác thường';
        }
    } else {
        ketqua = ' mời nhập vào cạnh > 0';
    }
    document.getElementById("bai4-result").innerHTML = ` ${ketqua}`;
}
